__author__='Elinor'
import os
import subprocess

def chop_to_top_1000(source_dir,dest_dir):
	for res_file in os.listdir( source_dir ):
		batch_eval1="more "+source_dir+res_file+""" | awk '{if ($4<=1000) print;}' > """+dest_dir+res_file
		output1=subprocess.check_output( batch_eval1, shell=True )
	return

def trec_eval_normal_on_1000(source_dir,dest_dir):
	trec_file="/lv_local/home/elinor/trec_eval.8.1/trec_eval"
	qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
	for res_file in os.listdir( source_dir ):
		batch_eval2=trec_file+" -q "+qrels_path+" "+source_dir+res_file+ " > "+dest_dir+res_file
		output1 = subprocess.check_output(batch_eval2, shell=True)
	return
