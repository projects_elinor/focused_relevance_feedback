__author__='Elinor'

import sys
import os
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
import subprocess
from cvloo import CVLOO_baseline_eval_folder as cvl

#scriptpath="/lv_local/home/elinor/pycharm/evaluation/"
#sys.path.append(os.path.dirname(scriptpath))


"""
this file takes the output of the singleneg and retrieve some of the data for them-
 1. separate eval folder to folders by fb docs
 2. cv loo for each
 3. taking the results into table
 4. greping top map and top P5 into table.
"""
init_ret_path_input='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
qrels_path_input='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'


#home_dir_input='/lv_local/home/elinor/negative_feedback/baselines/Fixed_RelSeg/'
reports_home='/lv_local/home/elinor/negative_feedback/reports/'
if not os.path.exists(reports_home ):
	os.makedirs( reports_home )
reports_home=reports_home+'/NRD_effect/'
if not os.path.exists( reports_home ):
	os.makedirs( reports_home )
if not os.path.exists( reports_home+'/cvloo_mean/' ):
	os.makedirs( reports_home+'/cvloo_mean/' )
#if not os.path.exists( '/lv_local/home/elinor/negative_feedback/reports/cvloo_dir/' ):
#	os.makedirs( '/lv_local/home/elinor/negative_feedback/reports/cvloo_dir/' )
if not os.path.exists(reports_home+'/cvloo_list/' ):
	os.makedirs(reports_home+'/cvloo_list/')

home_dir_input=sys.argv[1]+'/'
setup=home_dir_input.rstrip('/').rstrip('/').split('/')[-1].split('_')[0]
full_setup_name=home_dir_input.rstrip('/').rstrip('/').split('/')[-1]
model=home_dir_input.rstrip('/').rstrip('/').split('/')[-2]
#mode=sys.argv[2] #nrd == only nrd rerank. nrseg== only nrseg rerank

print full_setup_name
sampling=setup


flag_residual=True
print setup
if setup=="Fixed":
	flag_fixed=True
	max_K=10
else:
	flag_fixed=False
	max_K=5


def copy_split(source_dir,dest_dir):#dest_dir contains max_K dirs to which we will cpy
	for i in range(max_K):
		j=i+1
		batch_spltcp="scp "+source_dir+'/'+str(j)+'_* '+dest_dir+'/'+str(j)+'/ '
		output1=subprocess.check_output( batch_spltcp, shell=True )
	return 1;

def copy_split_nrd(source_dir,dest_dir):#dest_dir contains max_K dirs to which we will cpy
	for i in range(max_K):
		j=i+1
		batch_spltcp="scp "+source_dir+'/'+str(j)+'_*_NRD_0.0_NRSeg '+dest_dir+'/'+str(j)+'/ '
		output1=subprocess.check_output( batch_spltcp, shell=True )
	return 1;

def evalfile_to_query_scores_dict(measure_name,eval_file_path):
	dictQueryScore={}
	evalfile=open(eval_file_path,"r")
	for line in evalfile:
		array_line=line.split( )
		if array_line[0]==str(measure_name):
			if array_line[1]!='all':
				dictQueryScore[array_line[1]]=array_line[2].rstrip('\n')
	return dictQueryScore;

def build_ql_residual_map_p5():##goes to the residual directory and takes map all.
	global ql_map_dict
	global ql_p5_dict
	ql_map_dict={}
	ql_p5_dict={}
	for ql_eval_k in os.listdir(ql_residual_evald):
		j=ql_eval_k.split('_')[0]
		templ=[]
		dict_temp_map=evalfile_to_query_scores_dict('map',ql_residual_evald+'/'+ql_eval_k)
		for query in sorted(dict_temp_map.keys() ):
			templ+=[float(dict_temp_map[query])]
		ql_map_dict[str(j)]=templ
		templ=[]
		dict_temp_p5=evalfile_to_query_scores_dict( 'P5', ql_residual_evald+'/'+ql_eval_k )
		for query in sorted(dict_temp_p5.keys() ):
			templ+=[float(dict_temp_p5[query])]
		ql_p5_dict[str( j )]=templ
	return 1;


def cvloo_to_dir(source_dir):#dest_dir contains max_K dirs with eval files
	folder=temp_sep_fuse+'tempcvloo/'
	map_dict={}
	p5_dict={}
	cvloo_dict={}
	for eval_dir in os.listdir(source_dir):
		k=eval_dir
		temp_res=cvl.cv_loo(source_dir+eval_dir+'/',folder)
		map_dict[k]=temp_res['map']
		p5_dict[k]=temp_res['P5']
		cvloo_dict[k]=temp_res
		if os.path.exists( folder ):
			batch_rm="rm -fr "+folder
			outputmap=subprocess.check_output( batch_rm, shell=True )
	return cvloo_dict,map_dict,p5_dict;

def top_in_dir(source_dir):#dest_dir contains max_K dirs with eval files
	map_dict={}
	p5_dict={}
	for eval_dir in os.listdir(source_dir):
		for measure,d in zip(['map','"P5 "'],[map_dict,p5_dict]):
			batch_grep="grep "+measure+" "+source_dir+eval_dir+"""/* | grep all | sort -g -k3 | tail -n 1 |  awk '{print $3;}'"""
			val=subprocess.check_output( batch_grep, shell=True )
			batch_setup="grep "+measure+" "+source_dir+eval_dir+"""/* | grep all | sort -g -k3 | tail -n 1 |  awk '{print $1;}'"""
			setup=subprocess.check_output( batch_setup, shell=True )
			setup=setup.split(':')[-2].split('/')[-1]
			d[eval_dir]={}
			d[eval_dir]['setup']=setup
			d[eval_dir]['val']=val.rstrip('\n')
	#print "map", map_dict
	#print "p5", p5_dict
	return map_dict,p5_dict;



def define_folders_for_singleneg(home_dir_input):
	global residual_map_dict
	residual_map_dict={}
	global normal_map_dict
	normal_map_dict={}
	global residual_p5_dict
	residual_p5_dict={}
	global normal_cvloo_dict
	normal_cvloo_dict={}
	global residual_cvloo_dict
	residual_cvloo_dict={}
	global normal_p5_dict
	normal_p5_dict={}
	global home_baseline_dir
	global home_sig_dir
	global ql_residual_evald
	global fusion_residual_evald
	global fusion_normal_evald
	home_baseline_dir=home_dir_input
	home_sig_dir=home_dir_input+'single_neg/'
	ql_residual_evald=home_sig_dir+'/eval/QL_residual_eval/'
	fusion_residual_evald=home_sig_dir+'eval/res_1000_residual_eval/'
	fusion_normal_evald=home_sig_dir+'eval/res_1000_eval/'
	##dir to make
	dir=[]
	global temp_sep
	global temp_sep_fuse
	global temp_sep_fuse_normal
	global temp_sep_fuse_residual
	global normal_folders
	global residual_folders
	temp_sep=home_dir_input+'temp_sep/'
	temp_sep_fuse=temp_sep+rerank_setup_name+'/'
	temp_sep_fuse_normal=temp_sep_fuse+'normal/'
	temp_sep_fuse_residual=temp_sep_fuse+'residual/'
	normal_folders=[]
	residual_folders=[]##add the relative path
	for i in range(max_K):
		normal_folders+=[temp_sep_fuse_normal+str(i+1)+'/']
		residual_folders+=[temp_sep_fuse_residual+str(i+1)+'/']
	dir+=[temp_sep,temp_sep_fuse,temp_sep_fuse_normal,temp_sep_fuse_residual]
	dir+=normal_folders+residual_folders
	for folder in dir:
		print folder
		if not os.path.exists( folder ):
			os.makedirs( folder )
	return 1;

def define_folders_for_base(home_dir_input):
	global residual_map_dict
	residual_map_dict={}
	global normal_map_dict
	normal_map_dict={}
	global normal_cvloo_dict
	normal_cvloo_dict={}
	global residual_cvloo_dict
	residual_cvloo_dict={}
	global residual_p5_dict
	residual_p5_dict={}
	global normal_p5_dict
	normal_p5_dict={}
	global home_baseline_dir
	global ql_residual_evald
	global residual_evald
	global normal_evald
	home_baseline_dir=home_dir_input
	ql_residual_evald=home_baseline_dir+'eval/QL_residual_eval/'
	residual_evald=home_baseline_dir+'eval/residual_collection_eval/'
	normal_evald=home_baseline_dir+'eval/normal_eval/'
	##dir to make
	dir=[]
	global temp_sep
	global temp_sep_fuse
	global temp_sep_fuse_normal
	global temp_sep_fuse_residual
	global normal_folders
	global residual_folders
	rerank_setup_name='non'
	temp_sep=home_dir_input+'temp_sep/'
	temp_sep_fuse=temp_sep+rerank_setup_name+'/'
	temp_sep_fuse_normal=temp_sep_fuse+'normal/'
	temp_sep_fuse_residual=temp_sep_fuse+'residual/'
	if os.path.exists( temp_sep ):
		batch_rm="rm -fr "+temp_sep
		outputmap=subprocess.check_output( batch_rm, shell=True )
	normal_folders=[]
	residual_folders=[]##add the relative path
	for i in range(max_K):
		normal_folders+=[temp_sep_fuse_normal+str(i+1)+'/']
		residual_folders+=[temp_sep_fuse_residual+str(i+1)+'/']
	dir+=[temp_sep,temp_sep_fuse,temp_sep_fuse_normal,temp_sep_fuse_residual]
	dir+=normal_folders+residual_folders
	for folder in dir:
		print folder
		if not os.path.exists( folder ):
			os.makedirs( folder )
	return 1;



output_text='sampling,model,full_setup_name,fb_docs,rerank_setup,map_normal,p5_normal,map_residual,QLmap_residual,p5_residual,QLp5_residual,,'+'top_map_normal_setup,top_map_normal,top_p5_normal_setup,top_p5_normal,top_map_residual_setup,top_map_residual,top_p5_residual_setup,top_p5_residual,mapQLN,p5QLN'+'\n'
cvloo_grade_list='sampling,model,full_setup_name,fb_docs,rerank_setup,map_normal,p5_normal,map_residual,p5_residual,QL_map_residual,QL_p5_residual'+'\n'
for rerank_setup in ['non','single_neg']:
	if rerank_setup=='non':
		rerank_setup_name='non'
		define_folders_for_base( home_dir_input )
		build_ql_residual_map_p5( )
		copy_split( normal_evald, temp_sep_fuse_normal )
		copy_split( residual_evald, temp_sep_fuse_residual )
		normal_cvloo_dict,normal_map_dict, normal_p5_dict=cvloo_to_dir( temp_sep_fuse_normal )
		normal_map_top, normal_p5_top=top_in_dir( temp_sep_fuse_normal )
		residual_cvloo_dict,resi_map_dict, resi_p5_dict=cvloo_to_dir( temp_sep_fuse_residual )
		resi_map_top, resi_p5_top=top_in_dir( temp_sep_fuse_residual )
	else:
		rerank_setup_name=rerank_setup
		define_folders_for_singleneg(home_dir_input)
		build_ql_residual_map_p5()
		copy_split_nrd(fusion_normal_evald,temp_sep_fuse_normal)
		copy_split_nrd(fusion_residual_evald,temp_sep_fuse_residual)
		normal_cvloo_dict,normal_map_dict,normal_p5_dict=cvloo_to_dir(temp_sep_fuse_normal)
		normal_map_top, normal_p5_top=top_in_dir(temp_sep_fuse_normal)
		residual_cvloo_dict,resi_map_dict,resi_p5_dict=cvloo_to_dir(temp_sep_fuse_residual)
		resi_map_top,resi_p5_top=top_in_dir(temp_sep_fuse_residual)
	for i in range(max_K):
		j=str(i+1)
		cvloo_grade_list+=sampling+','+model+','+full_setup_name+','+str(j)+','+rerank_setup_name+',"'+str(normal_cvloo_dict[j]['map_Glist'])+'","'+str(normal_cvloo_dict[j]['P5_Glist'])+'","'+str(residual_cvloo_dict[j]['map_Glist'])+'","'+str(residual_cvloo_dict[j]['P5_Glist'])+'","'+str(ql_map_dict[j])+'","'+str(ql_p5_dict[j])+'"\n'
		output_text+=sampling+','+model+','+full_setup_name+','+str(j)+','+rerank_setup_name+','+normal_map_dict[j]+','+normal_p5_dict[j]+','+resi_map_dict[j]+','+resi_p5_dict[j]+','+str(sum(ql_map_dict[j])/float(len(ql_map_dict[j])))+','+str(sum(ql_p5_dict[j])/float(len(ql_p5_dict[j])))+',,'+normal_map_top[j]['setup']+','+normal_map_top[j]['val']+','+normal_p5_top[j]['setup']+','+normal_p5_top[j]['val']+','+resi_map_top[j]['setup']+','+resi_map_top[j]['val']+','+resi_p5_top[j]['setup']+','+resi_p5_top[j]['val']+',0.3684,0.6117\n'
file=open(reports_home+'/cvloo_mean/'+'cvloo_'+model+'_'+full_setup_name+'_fb.csv','w')
file.write(output_text)
file.close()
file=open(reports_home+'/cvloo_list/'+'cvloo_'+model+'_'+full_setup_name+'_fb.csv','w')
file.write(cvloo_grade_list)
file.close()

if os.path.exists( temp_sep ):
	batch_rm="rm -fr "+temp_sep
	outputmap=subprocess.check_output( batch_rm, shell=True )




