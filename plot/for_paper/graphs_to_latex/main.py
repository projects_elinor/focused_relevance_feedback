__author__='Yojik'
import os
"""
		\newcommand{\figWidtha}{5in}
		\newcommand{\figHeightaC}{3in}"""

plotdir="/lv_local/home/elinor/negative_feedback/reports/comparable_plots_residual_all/"
for dir in os.listdir(plotdir):
	if dir=="alternate":
		for file in os.listdir(plotdir+'/'+dir):
			if "eps" not in file:
				continue;
			figname=dir+'/'+file
			lable=file
			fig_begin="\\begin{figure}[htbp]\n\t\t\t\centering\n\t\t\t\epsfig{file="+figname+",width=\\figWidtha,height=\\figHeightaC} \n\t\t\t\caption{}\n\t\label{fig:"+lable+"}%\n\end{figure}%\n\n"
			print fig_begin