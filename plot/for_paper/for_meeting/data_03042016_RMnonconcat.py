__author__='Elinor'
import  matplotlib
import matplotlib.mathtext
matplotlib.mathtext.SHRINK_FACTOR=0.9
matplotlib.mathtext.GROW_FACTOR=1/0.9
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
import sys
scriptpath="/lv_local/home/elinor/pycharm/nf/"
sys.path.append(os.path.dirname(scriptpath))
from plot import plotfromdata as plt
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
#rc('text', usetex=True)


#matplotlib.pyplot.autoscale(enable=True, axis='both', tight=None)






def creat_plot_SMM_SMMP_norerank_all(plotsdir,df_all,residual_with_ql=0,with_segmentsSMMPP=0,with_SMMPP=0):#for SMM,distillation effect over RelD,RelSeg
	this_plot_homedir=plotsdir+'/Distil-effect-new-all/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		for evalmethod in ['normal','residual']:
			for measure in ['map','p5']:
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				# #now the setup is set. for each model should creat the table and plot
				a=df_all
				mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
				a=a[mask]
				a=a[(a['rerank_setup'] == 'non')]
				a['positive']='$R_{d}$'
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				a['positive'][(a['full_setup_name'] == 'RelSeg')]='$R_{p}$'
				a['distil'][(a['distil'] == 'NRD')]='$NR_{d}$'
				a['distil'][(a['distil'] == 'NRSeg')]='$NR_{p}$'
				a['distil'][(a['distil'] == 'NRD_NRSeg')]='$NR_{p},NR_{d}$'
				if not with_segmentsSMMPP:
					mask=a.apply( lambda x: not( 'SMMPP' in str( x ) and 'RelSeg' in str(x) ), axis=1 )
					a=a[mask]
				if not with_SMMPP:
					mask=a.apply( lambda x: not ( 'SMMPP' in str( x )  ), axis=1 )
					a=a[mask]
				print set(a['model'])
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMP']='Distill'+'('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMP-rerank']='RDistill'+'('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMM']='MM'+''+'('+a['positive']+')'
				a['unique_key']=a['model']#+' '+a['positive']
				if residual_with_ql:#evalmethod == 'residual' and
					measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
				else:
					measures_to_check=[measure+'_'+evalmethod]
				list_of_df_to_plot=[]
				for m in measures_to_check:
					# print a.columns
					#print m
					ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
					if 'QL' in m:
						ai['unique_key']=ai['unique_key'].apply( lambda x: '$D_{init}$' )
						ai=ai.drop_duplicates( )
						#print ai
						ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						#ai=ai.reindex(['Distill($R_{doc}$,$N_{doc}$)','Distill($R_{doc}$,$N_{psg}$)','Distill($R_{psg}$,$N_{doc}$)','Distill($R_{psg}$,$N_{psg}$)',"MM($R_{doc}$)","MM($R_{psg}$)",'$D_{init}$'])I once throw this away because it didn't work with (and worked without)
						#ai=ai.reindex(["MM($R_{doc}$)","MM($R_{por}$)",'$\\theta^q_{rel}$($R_{doc}$,$N_{doc}$)','$\\theta^q_{rel}$($R_{doc}$,$N_{por}$)','$\\theta^q_{rel}$($R_{por}$,$N_{doc}$)','$\\theta^q_{rel}$($R_{por}$,$N_{por}$)']) I once throw this away because it didn't work with (and worked without)
					else:
						ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						#ai=ai.reindex(['Distill($R_{d}$,$NR_{d}$)','Distill($R_{d}$,$NR_{p}$)','Distill($R_{p}$,$NR_{d}$)','Distill($R_{p}$,$NR_{p}$)',"MM($R_{d}$)","MM($R_{p}$)"])
					#print ai.index
					#print ai
					list_of_df_to_plot+=[ai]
				plotname='Distillation-effect-no-rerank'+'-'+measure+'-'+sampling+'-'+evalmethod+["",'-w-QL'][residual_with_ql]
				plot_headline='3C-SMM'+'-'+measure+'-'+sampling+'-'+evalmethod+'-Distillation-effect-no-rerank'
				plt.paper_plot(plot_headline,this_plot_homedir_samp,plotname,list_of_df_to_plot,ylable,0,"distilPlus")

def creat_plot_rm_compare(plotsdir, RMnonConcat,RMconcat):# for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	this_plot_homedir=plotsdir+'/RM_compare/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		this_plot_homedir_model=this_plot_homedir_samp
		if not os.path.exists( this_plot_homedir_model ):
			os.makedirs( this_plot_homedir_model )
		plotsdir_pos=this_plot_homedir_model+'/'
		if not os.path.exists( plotsdir_pos ):
			os.makedirs( plotsdir_pos )
		for evalmethod in ['normal', 'residual']:
			for measure in ['map']:
				list_of_df_to_plot=[]
				for pos in ['RelDP','RelSeg','RelD']:
					if measure == 'map':
						ylable='MAP'
					else:
						ylable='P5'
					# #now the setup is set. for each model should creat the table and plot
					add='$'
					c=RMconcat
					c['model']='RM3C'
					#add='^{uni}$)'
					b=RMnonConcat
					b['model']='RM3NC'
					a=pd.concat( [c,b], axis=0 )
					print 'lalal',a
					a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
					mask=a.apply( lambda x: sampling in str( x ), axis=1 )
					a=a[mask]

					if a.shape[0] == 0:
						continue
					#a=a[a['full_setup_name']==pos]
					#a=a[(a['full_setup_name'] != 'RelDP')]
					#a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='$D_{r}^{percentage}$'
					#print a
					a=a[(a['full_setup_name'] == pos)]
					a['full_setup_name']='$q_{pos}$('+pos+')'

					a['rerank_setup'][a['rerank_setup'] == 'non']='pos_only'
					a['rerank_setup'][a['rerank_setup'] == 'single_neg-combined_fuse_res']='$SN$_combined'
					a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRD_fus_res']='$SN$_NRD'
					a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRSeg_fus_res']='$SN$_NRSeg'
					a=a[(a['rerank_setup'] == '$SN$_NRSeg') | (a['rerank_setup'] == '' ) |(a['rerank_setup'] == '$SN$_combined' ) | (a['rerank_setup'] == '$SN$_NRD' )]
					a['unique_key']=a['model']+' '+a['rerank_setup']+' '+a['full_setup_name']
					measures_to_check=[measure+'_'+evalmethod]
					for m in measures_to_check:
						#print a.columns
						#print m
						ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
						print ai
						if 'QL' in m:
							ai['unique_key']=ai['unique_key'].apply( lambda x: '$D_{init}$' )
							ai=ai.drop_duplicates( )
							# print ai
							ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						else:
							#ai['unique_key']=ai['unique_key'].apply( lambda x: '$'+pos+'$  '+str( x ) )
						#ai=ai.drop_duplicates( )
						#print ai
							ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						#print ai.index
						list_of_df_to_plot+=[ai]

				plotname='single-multi-compare'+'-'+measure+'-'+sampling+'-'+evalmethod
				plot_headline='single-multi-compare'+'-'+measure+'-'+sampling+'-'+evalmethod+'-rerank-over-all'
				plt.paper_plot( plot_headline, plotsdir_pos, plotname, list_of_df_to_plot, ylable, 0,'RM' )


#########################################################################################
#########################################################################################
def main():
	plotsdir=plotsdirout='/lv_local/home/elinor/negative_feedback/reports/comparable_plots_residual_all/'
	datasdirout='/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots_residual_all/'
	#csv_source='/lv_local/home/elinor/negative_feedback/reports/191115extra_list_all_clean.csv'#this is from before caching the reranking cvloo to simple aggregation
	csv_source="/lv_local/home/elinor/negative_feedback/reports/27032016_new_learning/27032016/data_27032016_new_learning.csv"
	csv_source2="/lv_local/home/elinor/negative_feedback/reports/data_03042016_RMnonconcat.csv"

	SMM, SMM_all, SMMP, SMMPP, RMconcat, all_table=plt.read_and_proccess( plotsdirout, datasdirout, csv_source, 0 )
	#print RMconcat
	SMM, SMM_all, SMMP, SMMPP, RMnonConcat, all_table=plt.read_and_proccess( plotsdirout, datasdirout, csv_source2, 0 )
	#print RMnonConcat
	creat_plot_rm_compare( plotsdir, RMconcat, RMnonConcat)

	#

if __name__ == '__main__':
	main()