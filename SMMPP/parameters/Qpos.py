__author__='Elinor'
"""
this has been modified for SMMPP (plus plus) (4)
and the number of fbdocs should be constant and bigger then the number of used fb docs.
Fbdocs passed are all(both relevant and non-relevant)
parameter file name should interpret as:
str(fbDocs_parm)+'_'+str(fbOrigWeight_parm)+'_'+str(lambda1)+'_'+str(lambda2)+'_'+str(lambda3)+'_'+str(term)+'_'+feedback_setup_name
lambda1-non-relevant segments
lambda2-non-relevant documents
lambda3- corpus
"""
import os
import os.path
import sys
import subprocess
import itertools
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from qpos_eval import eval_settings as es
#define paths
#baseline_home_dir="/lv_local/home/elinor/negative_feedback/baselines/Accumulative_RelSeg"+'/'
print sys.argv
if len(sys.argv)>1:
	baseline_home_dir=sys.argv[1]+'/'
else:
	list_setups=['Accumulative_RelD_NRD_NRSeg','Fixed_RelD_NRD_NRSeg','Fixed_RelSeg_NRD_NRSeg','Accumulative_RelSeg_NRD_NRSeg']
	for dirname in list_setups:
		baseline_home_dir='/lv_local/home/elinor/negative_feedback/SMMPP/'+dirname+'/'
		if not os.path.exists( baseline_home_dir ):
			os.makedirs( baseline_home_dir )


temp=baseline_home_dir.rstrip('/').rstrip('/').split('_')
feedback_setup_name=temp[-3]+'_'+temp[-2]+'_'+temp[-1]
setup=baseline_home_dir.rstrip('/').rstrip('/').split('_')[-4].split('/')[-1]
print feedback_setup_name;
print setup

if setup=="Fixed":
	print "Fixed"
	flag_fixed=True
	max_K=10
else:
	print "NonFixed"
	flag_fixed=False
	max_K=5

ROBUSTflag=0
if "robust" in baseline_home_dir:
	ROBUSTflag=1;
if ROBUSTflag:
	start_param_file_path="/lv_local/home/elinor/negative_feedback_robust/robust_qrelsAndParams/param_part1_keeping_forSMMPP.xml"
	orig_queries="/lv_local/home/elinor/negative_feedback_robust/robust_qrelsAndParams/fake_segments_queries.xml"
	init_ret_path='/lv_local/home/elinor/negative_feedback_robust/QL/QL_robust_1500.txt'
	qrels_path='/lv_local/home/elinor/negative_feedback_robust/robust_qrelsAndParams/qrelsROBUST'
else:
	start_param_file_path="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/param_part1_keeping_forSMMPP.xml"
	orig_queries="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/INEX_params_b_segQ.xml"
	init_ret_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
	qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
output_param_files_dir=baseline_home_dir+"parameters/"
##any number bigger than the amount of docs we are using for feedback
CONST_fbdoc_num=2*(max_K+1)
fbDocs=range(1,max_K+1)
fbOrigWeight=[0.2,0.5,0.8]
##this is lambda 1 2 3
weights=[0,0.1,0.5,0.9]
fbTerms=[10,25,50]


def create_dir():
	if not os.path.exists(baseline_home_dir):
		os.makedirs(baseline_home_dir)
	if not os.path.exists(output_param_files_dir):
		os.makedirs(output_param_files_dir)
	if not os.path.exists(baseline_home_dir+"res/"):
		os.makedirs(baseline_home_dir+"res/")
	if not os.path.exists(baseline_home_dir+"chunks/"):
		os.makedirs(baseline_home_dir+"chunks/")




def read_query_info(orig_queries):
	#step one- for each query create a dictionary of the query words.
	global q_info
	q_info={}
	q_file=open(orig_queries,'r')
	query_str_a='\t<query>\n'
	query_str_b=str()
	for line in q_file:
		if 'number' in line:
			q_id=line.strip('\t\t<number>')
			q_id=q_id.rstrip('</number>\n\t\t')
			query_str_a+=line;
		if '<text>' in line:
			query_str_a+=line;
		if '<relseg>' in line:
			query_str_b+=line;
		if '</query>' in line:
			query_str_b+=line;
			q_info[q_id]=[query_str_a,query_str_b]
			query_str_a='\t<query>\n'
			query_str_b=str()
	q_file.close()
	return


def create_params_baseline_accumulative():
	read_query_info(orig_queries)
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, qrels_path )
	create_dir()
	#start code:
	#i=0;
	start_param_file=open(start_param_file_path,'r')
	start_param=start_param_file.read()
	start_param_file.close()
	for fbDocs_parm in fbDocs:
		for fbOrigWeight_parm in fbOrigWeight:
			for lambda1 in weights:
				for lambda2 in weights:
					for lambda3 in weights:
						mixtureCorpusWeight_param_t=lambda1+lambda2+lambda3 # lambda bg
						if mixtureCorpusWeight_param_t < 1:
							mixtureNegWeight_param_t=0 if ((lambda1+lambda2) == 0) else ((lambda1+lambda2)/mixtureCorpusWeight_param_t)
							mixtureNegDocsWeight_param_t=0 if (lambda2 == 0) else (lambda2/(lambda1+lambda2))
							print '['+str(lambda1)+','+str(lambda2)+','+str(lambda3)+']\n'
							for term in fbTerms:
								#writing parameter file part 1
								this_output=str()
								this_output_name=str( fbDocs_parm )+'_'+str( fbOrigWeight_parm )+'_'+str( lambda1 )+'_'+str( lambda2 )+'_'+str( lambda3 )+'_'+str( term )+'_'+feedback_setup_name
								this_output_path=output_param_files_dir+this_output_name
								this_output+=start_param
								this_output+='\t<SMMPsetup>'+feedback_setup_name+'</SMMPsetup>\n'
								this_output+='\t<mixtureCorpusWeight>'+str(mixtureCorpusWeight_param_t)+'</mixtureCorpusWeight>\n'
								this_output+='\t<mixtureNegWeight>'+str(mixtureNegWeight_param_t)+'</mixtureNegWeight>\n'
								this_output+='\t<mixtureNegDocsWeight>'+str(mixtureNegDocsWeight_param_t)+'</mixtureNegDocsWeight>\n'
								this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
								this_output+='\t<fbOrigWeight>'+str(fbOrigWeight_parm)+'</fbOrigWeight>\n'
								this_output+='\t<fbDocs>'+str(CONST_fbdoc_num)+'</fbDocs>\n'
								for query in sorted(q_info.keys()):
									this_output+=q_info[query][0]
									for docNo in es.All_used[fbDocs_parm][query]:
										this_output+='\t\t<feedbackDocno>'+str(docNo)+'</feedbackDocno>\n'
									this_output+=q_info[query][1]
								this_output+="</parameters>\n"
								out_f=open(this_output_path,'w+')
								out_f.write(this_output)
								out_f.close()
		print "done", fbDocs_parm
	return 1



def create_params_baseline_fixed():
	read_query_info(orig_queries)
	create_dir()
	#start code:
	start_param_file=open(start_param_file_path,'r')
	start_param=start_param_file.read()
	start_param_file.close()
	for fbDocs_parm in fbDocs:
		for fbOrigWeight_parm in fbOrigWeight:
			for lambda1 in weights:
				for lambda2 in weights:
					for lambda3 in weights:
						mixtureCorpusWeight_param_t=lambda1+lambda2+lambda3 #lambda bg
						if mixtureCorpusWeight_param_t < 1:
							mixtureNegWeight_param_t= 0 if ((lambda1+lambda2)==0) else ((lambda1+lambda2)/mixtureCorpusWeight_param_t)
							mixtureNegDocsWeight_param_t=0 if (lambda2==0) else (lambda2/(lambda1+lambda2))
							print '['+str(lambda1)+','+str(lambda2)+','+str(lambda3)+']\n'
							for term in fbTerms:
								#writing parameter file part 1
								this_output=str()
								this_output_name=str(fbDocs_parm)+'_'+str(fbOrigWeight_parm)+'_'+str(lambda1)+'_'+str(lambda2)+'_'+str(lambda3)+'_'+str(term)+'_'+feedback_setup_name
								this_output_path=output_param_files_dir+this_output_name
								this_output+=start_param
								this_output+='\t<SMMPsetup>'+feedback_setup_name+'</SMMPsetup>\n'
								this_output+='\t<mixtureCorpusWeight>'+str(mixtureCorpusWeight_param_t)+'</mixtureCorpusWeight>\n'
								this_output+='\t<mixtureNegWeight>'+str(mixtureNegWeight_param_t)+'</mixtureNegWeight>\n'
								this_output+='\t<mixtureNegDocsWeight>'+str(mixtureNegDocsWeight_param_t)+'</mixtureNegDocsWeight>\n'
								this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
								this_output+='\t<fbOrigWeight>'+str(fbOrigWeight_parm)+'</fbOrigWeight>\n'
								this_output+='\t<fbDocs>'+str(fbDocs_parm)+'</fbDocs>\n'
								for query in sorted(q_info.keys()):
									this_output+=q_info[query][0]
									this_output+=q_info[query][1]
								this_output+="</parameters>\n"
								out_f=open(this_output_path,'w+')
								out_f.write(this_output)
								out_f.close()
		print "done", fbDocs_parm
	return 1

if flag_fixed:
	create_params_baseline_fixed();
else:
	create_params_baseline_accumulative();

"""
use to be
create_params_baseline_fixed();

create_params_baseline_accumulative();

since there is fixed flag, and control in the indri code, no hurm- results for the same parameters are equal. (tested)"""