__author__='Elinor'
__author__='Elinor'
import os
import sys
import subprocess
import itertools
import pandas as pd
import pickle


def move(source_dir, dest_dir):#dest_dir contains max_K dirs to which we will cpy
	print source_dir
	if not os.path.exists( dest_dir ):
		os.makedirs( dest_dir )
	batch_spltcp="mv "+source_dir+'/*_avg/ '+dest_dir+'/'
	#print batch_spltcp
	output1=subprocess.check_output( batch_spltcp, shell=True )
	return 1;

def remove_avg_sefa(dest_dir):#dest_dir contains max_K dirs to which we will cpy
	print dest_dir
	for dir in os.listdir( dest_dir ):
		batch_spltcp="mv "+dest_dir+'/'+dir+'/ '+dest_dir+'/'+dir.rstrip('/').rstrip('_avg')+'/'
		#print batch_spltcp
		output1=subprocess.check_output( batch_spltcp, shell=True )
	return 1;


def compare_rank(file1,file2):
	df1=pd.read_csv(file1,sep=' ',index_col=False,skipinitialspace=True,names=['qid','Q0','docID_noexp','rank','score','indri'] )
	df1=df1[['qid','rank','docID_noexp']].set_index(['qid','rank'])
	df2=pd.read_csv(file2,sep=' ',index_col=False,skipinitialspace=True,names=['qid','Q0','docID_exp','rank','score','indri'] )
	df2=df2[['qid','rank','docID_exp']].set_index(['qid','rank'])
	cat=pd.concat( [df1, df2], axis=1 )
	cat['equal']=cat['docID_noexp']==cat['docID_exp']
	print cat
	#print (df1 != df2).stack( )


homedir='/lv_local/home/elinor/negative_feedback/'
test_dir="/lv_local/home/elinor/test_exp/"
for RM in os.listdir(test_dir):
	print RM
	for setup in os.listdir(test_dir+'/'+RM+'/'):
		real_res_dir=homedir+'/'+RM+'/'+setup+'/multi_neg/combined_fuse_res/res_1000/'
		print real_res_dir
		if os.path.exists( real_res_dir):
			non_exp_dir=test_dir+'/'+RM+'/'+ setup+'/combined_fuse_res/res_1000/'
			print non_exp_dir
			for file in os.listdir(non_exp_dir):
				compare_rank(non_exp_dir+file,real_res_dir+file)
