__author__='Elinor'
import os
import sys
import subprocess
import itertools
import pandas as pd
import pickle


def move(source_dir, dest_dir):#dest_dir contains max_K dirs to which we will cpy
	print source_dir
	if not os.path.exists( dest_dir ):
		os.makedirs( dest_dir )
	batch_spltcp="mv "+source_dir+'/*_avg/ '+dest_dir+'/'
	#print batch_spltcp
	output1=subprocess.check_output( batch_spltcp, shell=True )
	return 1;

def remove_avg_sefa(dest_dir):#dest_dir contains max_K dirs to which we will cpy
	print dest_dir
	for dir in os.listdir( dest_dir ):
		batch_spltcp="mv "+dest_dir+'/'+dir+'/ '+dest_dir+'/'+dir.rstrip('/').rstrip('_avg')+'/'
		#print batch_spltcp
		output1=subprocess.check_output( batch_spltcp, shell=True )
	return 1;

homedir='/lv_local/home/elinor/negative_feedback/RM/'
for setup in os.listdir(homedir):
	print setup
	source=homedir+'/'+setup+'/multi_neg/'
	dest=homedir+'/'+setup+'/multi_neg_avg/'
	move(source, dest)
	remove_avg_sefa(dest)
	single_dest=homedir+'/'+setup+'/single_neg/combined_fuse_res/'
	single_source=homedir+'/'+setup+'/single_neg/'
	if not os.path.exists( single_dest ):
		os.makedirs( single_dest )
		batch_spltcp="mv "+single_source+'/*/ '+single_dest+'/'
		# print batch_spltcp
		output1=subprocess.check_output( batch_spltcp, shell=True )